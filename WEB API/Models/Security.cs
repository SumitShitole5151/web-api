﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEB_API.DAL;

namespace WEB_API.Models
{
    public class Security
    {
        public static bool Login(string UserName,string Password)
        {
             bool Flag= false;
            using(WebApiDBEntities1 DB= new WebApiDBEntities1())
            {
                Flag =Convert.ToBoolean(DB.spLogin(UserName, Password).FirstOrDefault());
            }
            return Flag;
        }
    }
}