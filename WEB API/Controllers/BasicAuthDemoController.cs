﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WEB_API.DAL;
using WEB_API.Filters;

namespace WEB_API.Controllers
{
    public class BasicAuthDemoController : ApiController
    {
       [BasicAuthentication]
        public HttpResponseMessage Get()
        {
            string UserName = Thread.CurrentPrincipal.Identity.Name;
            using (WebApiDBEntities1 DB = new WebApiDBEntities1())
            {
                switch (UserName.ToLower())
                {
                    case "sumit":
                        return Request.CreateResponse(HttpStatusCode.OK,
                            DB.tblRawDatas.Where(x => x.Gender == "Male").ToList());

                    case "varsha":
                        return Request.CreateResponse(HttpStatusCode.OK,
                        DB.tblRawDatas.Where(x => x.Gender == "Female").ToList());

                    default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
            }          
        }
    }
}